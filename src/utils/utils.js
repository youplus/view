import { YOUPLUS_CONFIG } from './const'

export function getVideoURL(video) {
	//if property video_url exists use it else use getVideoAPI + video_id method
	return video.hasOwnProperty('video_url')
		? video.video_url
		: YOUPLUS_CONFIG.get_video_url + video.video_id
}

export function isEmailValid(email) {
	return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)
}

export function getYpKey() {
	return window.location.href.split('?')[0]
}

export function generateThumbnail(video) {
	var canvas = document.createElement('canvas')
	;(canvas.width = video.videoWidth), (canvas.height = video.videoHeight)
	canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height)
	var tb,
		dataURI = canvas.toDataURL(),
		byteString = atob(dataURI.split(',')[1]),
		ab = new ArrayBuffer(byteString.length),
		ia = new Uint8Array(ab)
	for (var i = 0; i < byteString.length; i++) ia[i] = byteString.charCodeAt(i)
	tb = new Blob([ab], {
		type: dataURI
			.split(',')[0]
			.split(':')[1]
			.split(';')[0]
	})
	canvas.remove()
	video.pause()
	// video.src = "";
	return tb
}

// prompt the user to grant video permissions if not already granted
export function captureUserMedia(callback) {
	var params = { audio: true, video: true }
	if (navigator.getUserMedia === undefined) {
		callback(null)
	} else {
		navigator.getUserMedia(
			params,
			stream => {
				callback(stream)
			},
			error => {
				if (error) {
					console.log(error)
					callback(null)
					switch (error.name) {
					case 'PermissionDeniedError':
						alert(
							'Camera access blocked. Please go into your site settings and allow access.'
						)
						break
					case 'TrackStartError':
					case 'NotReadableError':
						alert(
							'Direct webcam access is not allowed. Please try with "Upload Answer" option below.'
						)
						break
					}
				}
			}
		)
	}
}
