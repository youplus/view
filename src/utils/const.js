export const YOUPLUS_CONFIG = {
  upload_url: 'https://ind-sapi.yupl.us/v1.7/app/live_opinion/widget_upload',
  report_url:
    'https://ind-sapi.yupl.us/v1.7/app/live_opinion/widget_video_report',
  widget_url: 'https://widget.youplus.com/w/widget.html?link_url=',
  get_contents_url:
    'https://ind-sapi.yupl.us/v1.7/app/content/contents_for_widgets?url=',
  get_video_url:
    'https://ind-sapi.yupl.us/v1.7/app/live_opinion/get_video?video_id=',
  get_questions_url:
    'https://ind-sapi.yupl.us/v1.7/app/live_opinion/get_questions?url=',
  source_image_url:
    'http://s3-ap-southeast-1.amazonaws.com/ind-ypassets/logos/youplus_logo.png'

  //widget_key: window.location.href,
  //widget_key: 'https://widget.youplus.com/infibeam/oneplus.html',
};

export const THUMBNAILS_CDN = 'https://df0bl3rlibrpg.cloudfront.net/';

export const PLAY_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/play-ico.png';
export const REC_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/record-btn.png';
export const LEFT_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/left-arrow.png';
export const RIGHT_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/right-arrow.png';
export const RETAKE_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/record-rerecord-btn.png';
export const RECORD_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/record-start-btn.png';
export const STOP_IMG =
  'https://d1uck549nef0ok.cloudfront.net/img/record-stop-btn.png';
