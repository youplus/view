import { YOUPLUS_CONFIG } from './const'
import { getYpKey, generateThumbnail } from './utils'

function uploadOpinion(opinionObject, callback) {
	const fileName = Math.round(Math.random() * 99999999) + 99999999
	const videoFile = new Blob(opinionObject.recordedBlobs, {
		type: 'video/webm'
	})
	const thumbnailBlob = generateThumbnail(
		document.getElementById('webcam-video')
	)
	var formData = new FormData()
	formData.append('video', new File([videoFile], fileName + '.mp4'))
	formData.append('thumbnail', new File([thumbnailBlob], fileName + '.jpg'))
	formData.append('type', 'video/mp4')
	formData.append('user_email', opinionObject.email)
	formData.append('is_widget', true)
	formData.append('widget_url', getYpKey())
	formData.append('taxonomy_path', opinionObject.taxonomy)
	fetch(YOUPLUS_CONFIG.upload_url, {
		method: 'post',
		body: formData
	})
		.then(parseJSON)
		.then(callback)
}

function parseJSON(response) {
	return response.data
}

const Api = {
	uploadOpinion
}

export default Api
