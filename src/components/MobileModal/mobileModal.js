import React from 'react'

import CloseIcon from '../../static/close.svg'
import './__style.css'

class Modal extends React.Component {
	render() {
		// Render nothing if the "show" prop is false
		if (!this.props.show) {
			return null
		}

		return (
			<div className="backdrop">
				<div className="modal">
					<div className="modal-close">
						<img src={CloseIcon} alt="Close" onClick={this.props.onClose} />
					</div>
					{this.props.children}
				</div>
			</div>
		)
	}
}

export default Modal
