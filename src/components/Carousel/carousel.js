import React from 'react'

export default class CarouselSlider extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			position: 0
		}
	}

  getOrder = itemIndex => {
  	const { position } = this.state
  	const { children } = this.props
  	const numItems = children.length || 1

  	if (itemIndex - position < 0) {
  		return numItems - Math.abs(itemIndex - position)
  	}
  	return itemIndex - position
  };

  render() {
  	const { children, title } = this.props
  	return (
  		<div>
  			<h2>{title}</h2>
  			<div
  				className="carousel-wrapper"
  				style={{ width: '100%', overflow: 'hidden' }}
  			>
  				<div
  					className="carousel-container"
  					style={{ display: 'flex', margin: '0 0 20px 20px' }}
  				>
  					{/* {children} */}
  					{children.map((child, index) => {
  						return (
  							<div
  								className="carousel-slot"
  								key={index}
  								order={this.getOrder(index)}
  								style={{
  									flex: '0 0 300px',
  									marginRight: '20px',
  									order: this.getOrder(index)
  								}}
  							>
  								{child}
  							</div>
  						)
  					})}
  				</div>
  			</div>
  		</div>
  	)
  }
}
