import React from 'react'
import AliceCarousel from 'react-alice-carousel'
import ArrowLeft from '../../static/arrow-left.png'
import ArrowRight from '../../static/arrow-right.png'
import Play from '../../static/play-ico.png'
import Loader from '../Loader/loader'

import './__style.css'

export default class Playlist extends React.PureComponent {
	onClick = video => {
		this.props.onClick(video)
	}

	render() {
		const {
			data,
			video,
			isDesktop,
			width,
			marginLeft,
			marginRight,
			marginBottom,
			responsive
		} = this.props

		let cards = []
		let key
		data && data.forEach((data, i) => {
			const { question } = data.question
			const { thumbnail_image_url, q_tags_id } = data.video

			// To figure out the strat index in the Alice Carousel
			if(video.q_tags_id === q_tags_id) {
				key = i
			}

			//Items to be shown in the Alice carousel
			cards.push(
				<div className="player_card" style={{ width: isDesktop ? 150 : 100, margin: 2 }}>
					<div
						className="player_card_heading"
						style={{
							color: '#e0e0e0',
							whiteSpace: !isDesktop ? 'nowrap' : '',
							textOverflow: 'ellipsis',
							overflow: 'hidden',
							fontSize: 'small',
							marginBottom: 2,
							lineHeight: isDesktop ? '1.1em' : '',
							height: isDesktop ? '3em' : '',
							textAlign: 'center'
						}}>
						{question}
					</div>
					<div className="play_card_image">
						<img
							alt="card"
							src={thumbnail_image_url}
							style={{ width: isDesktop ? 150 : '15vh', height: isDesktop ? 100 : '10vh', opacity: video.q_tags_id === q_tags_id ? '0.5' : ''}}
						/>
						{video.q_tags_id !== q_tags_id && (
							<img
								alt="Play"
								onClick={() => this.onClick(data.video)}
								src={Play}
								style={{
									width: 50,
									position: 'absolute',
									top: isDesktop ? '25%' : '10%',
									left: isDesktop ? '30%' : '25%',
									cursor: 'pointer'
								}}
							/>
						)}

						{/* If the current Video playing is the current card item */}
						{video.q_tags_id === q_tags_id && (
							<div className="playlist_nowPlaying" style={{ top: isDesktop ? '35%' : '20%'}}>
										Now Playing...
							</div>
						)}
					</div>
				</div>
			)
		})

		if (!data) {
			for (let i = 0; i < 4; i++) {
				cards.push(<Loader />)
			}
		}
		return (
			<div style={{ backgroundColor: 'black' }}>
				<div className="playlist_carousel"
					style={{
						display: 'flex',
						width: width,
						marginRight: marginRight,
						marginLeft: marginLeft,
						marginBottom: marginBottom, 
					}}>
					{isDesktop && (
						<img
							className="playlist_arrows"
							src={ArrowLeft}
							onClick={() => this.Carousel._slideNext()}
							alt="Left Arrow"
						/>
					)}
					<AliceCarousel
						items={cards}
						responsive={responsive}
						dotsDisabled={true}
						buttonsDisabled={true}
						ref={el => (this.Carousel = el)}
						keysControlDisabled={true}
						startIndex={key}
					/>
					{isDesktop && (
						<img
							className="playlist_arrows"
							src={ArrowRight}
							onClick={() => this.Carousel._slidePrev()}
							alt="Right Arrow"
						/>
					)}
				</div>
			</div>
		)
	}
}
