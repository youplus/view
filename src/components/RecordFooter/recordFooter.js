import React from 'react'

import YouplusLogo from '../../static/youplus-logo-black.png'
import RecordButton from '../../static/record-btn.png'

import './__style.css'

export default class RecordFooter extends React.PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			isHover: false
		}
	}

  onClick = () => {
  	this.props.onRecord()
  };

  toggleHover = () => {
  	console.log('Animation Starts here', this.state)
  	this.setState({
  		isHover: !this.state.isHover
  	})
  };

  render() {
  	const { isHover } = this.state
  	const ctaClassname = isHover ? 'widget-cta-hover' : 'widget-cta'
  	return (
  		<div className="record_align-center">
  			<div className="widget-divider" />
  			<div className="record_youplus-logo">
  				<span className="record_poweredBy">powered by </span>
  				<img alt="logo" src={YouplusLogo} width="75px" />
  			</div>
  			<div
  				onMouseEnter={this.toggleHover}
  				onMouseLeave={this.toggleHover}
  				onClick={this.onClick}
  				className={ctaClassname}
  			>
          What's your opinion?
  				<img
  					className="opinion_record_button"
  					src={RecordButton}
  					alt="Record"
  				/>
  			</div>
  		</div>
  	)
  }
}
