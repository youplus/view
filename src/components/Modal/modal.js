import React from 'react'

import CloseIcon from '../../static/close.svg'
import BlackCloseIcon from '../../static/closeBlack.svg'

import './__style.css'

class Modal extends React.Component {
	render() {
		const { show, isDesktop } = this.props
		// Render nothing if the "show" prop is false
		if (!show) {
			return null
		}

		return (
			<div className="desktop_backdrop">
				<div className="desktop_modal">
					<div className="desktop_modal-close">
						{isDesktop && (
							<img onClick={this.props.onClose} src={CloseIcon} alt="Close" />
						)}
						{!isDesktop && (
							<img
								onClick={this.props.onClose}
								src={BlackCloseIcon}
								alt="Close"
							/>
						)}
					</div>
					{this.props.children}
				</div>
			</div>
		)
	}
}

export default Modal
