import React from 'react'

import Play from '../../static/play-ico.png'
import './loader.css'
export default class Loader extends React.PureComponent {
	render() {
		return (
			<div>
				<div className="loader-card">
					<div className="loader-card-body loader-card-image">
						<img
							className="widget-play_btn"
							alt="Play"
							src={Play}
						/>
					</div>
					<div className="loader-card-container">
						<div className="loader-card-body loader-card-container-thumbnail"/>
						<div className="loader-card-container-details">
							<div className="loader-card-body loader-card-container-details-name"/>
							<div className="loader-card-body loader-card-container-details-rating"/>
							<div className="loader-card-body loader-card-container-details-options">
								<div className="views" />
								<div className="duration" />
							</div>
						</div>
					</div>
					<div className="loader-card-body loader-card-description" />
				</div>
			</div>
		)
	}
}
