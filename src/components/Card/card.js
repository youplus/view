import React from 'react'
import Play from '../../static/play-ico.png'
import Star from '../../static/star-outline.svg'
import StarFilled from '../../static/star.svg'

import './__style.css'

export default class Card extends React.PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			isPlayHover: false,
			isThumbnailCompressed: true,
			isProfilePicCompressed: true,
		}
	}

	onEnded = () => {
		this.props.onVideoEnded()
	}

	_showVideoModal = () => {
  		const { data } = this.props
  		this.props.onPlay(data)
	};

	toggleHover = () => {
		this.setState({
			isPlayHover: !this.state.isPlayHover
		})
	};

	onThumbnailError = () => {
	  this.setState ({
  		isThumbnailCompressed: false
	  })
	}

  onProfilePicError = () => {
  	this.setState({
  		isProfilePicCompressed: false
  	})
  }

  // TODO: Take comporessed images for thumbnail_image_url & user_profile_picture with fallback incase of noresponse
  render() {
  	const { isPlayHover, autoPlay } = this.state
  	const { data, playVideo } = this.props
  	const { question, video_info } = data
  	let filledStar = []
  	let outlineStar = []
  	for (let i = 0; i < video_info[0].rating_by_user; i++) {
  		filledStar.push(
  			<img className="icon_star" src={StarFilled} alt="Ratings" key={i} />
  		)
  	}
  	for (let i = 0; i < 5 - video_info[0].rating_by_user; i++) {
  		outlineStar.push(
  			<img className="icon_star" src={Star} alt="Ratings" key={i} />
  		)
  	}
	  
  	const compressedThumbnail = video_info[0].thumbnail_image_url.replace(/\.(?:jpg|gif|png)/g, '_nor.jpg')
  	const compressedProfilePic = video_info[0].user_profile_picture.replace(/\.(?:jpg|gif|png)/g, '_nor.jpg')
	  
  	return (
  		<div className="card">
  			<div className="card-image">
  				{!playVideo && (<div>
  					<img
  						className="card-image-thumbnail"
  						alt="Source Background..."
  						src={this.state.isThumbnailCompressed ? compressedThumbnail : video_info[0].thumbnail_image_url}
  						onError={this.onThumbnailError}
  					/>
  					<img
  						className="widget-play_btn"
  						onMouseEnter={this.toggleHover}
  						onMouseLeave={this.toggleHover}
  						alt="Play"
  						src={Play}
  						onClick={this._showVideoModal}
  					/>
  				</div>
  				)}
  				{playVideo && (
  					<video style={{height:200, width:'100%'}} controls autoPlay onClick={this._showVideoModal} onEnded={this.onEnded}>
  						<source src={video_info[0].video_url} />
					  </video>
  				)}
  			</div>
  			<div className="card-container">
  				<img
				  	className="card-container-thumbnail"
  					src={this.state.isProfilePicCompressed ? compressedProfilePic : video_info[0].user_profile_picture}
  					alt="Thumbnail alt"
  					onError={this.onProfilePicError}  
  				/>
  				<div className="card-container-details">
  					<div className="card-container-details-name">
  						<span>{video_info[0].user_name}</span>
  					</div>
  					<div className="card-container-details-rating">
  						{filledStar}
  						{outlineStar}
  					</div>
  					<div className="card-container-details-options">
  						<div className="views">
                			views:{' '}
  							<strong style={{ color: 'black' }}>
  								{video_info[0].view_count}
  							</strong>
  						</div>
  						<div
                				className="duration"
                				style={{ float: 'right', marginRight: '40px' }}
              				>
  							<strong style={{ color: 'black' }}>
  								{video_info[0].duration || '00:15'}
  							</strong>
              			</div>
  					</div>
  				</div>
  			</div>
  			<div className="card-description">
  				{question}
  			</div>
  		</div>
  	)
  }
}
