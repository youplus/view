import React from 'react'

import Modal from '../components/Modal/modal'
import Webcam from '../view/Webcam/webcam'

export default class DesktopRecord extends React.Component {
    onClose = () => {
    	this.props.onClose()
    }

    render() {
    	const { show } = this.props
    	return (
    		<Modal show={show} onClose={this.onClose} isDesktop={false}>
    			<Webcam taxonomy={this.props.taxonomy} />
    		</Modal>
    	)
    }
}