import React from 'react'
import PhoneModal from '../components/MobileModal/mobileModal'
import Webcam from '../view/Webcam/webcam'

export default class MobileRecord extends React.Component{
    onClose = () => {
    	this.props.onClose()
    }

    render() {
    	const { show } = this.props
    	return (
    		<PhoneModal show={show} onClose={this.onClose}>
    			<Webcam taxonomy={this.props.taxonomy} />
    		</PhoneModal>
    	)
    }
}