import React from 'react'
import './webcam.css'
import { captureUserMedia, isEmailValid } from '../../utils/utils'
import Api from '../../utils/api'

export default class Webcam extends React.Component {
	messages = {
		initialMessage:
			'Make it easy for us to reach out to you by providing your email address',
		invalidEmail: 'Please provide a valid email address',
		uploadedOpinionSuccess:
			'You will receive a notification via mail once your opinion is live.',
		uploading: 'Uploading...',
		thankYou: 'Thank you for your opinion! ✓'
	}

	state = {
		src: null,
		isMuted: true,
		isRecording: false,
		isUploaded: false,
		isUploading: false,
		webcamFailed: false,
		recordingStream: null,
		previewStream: null,
		recordingTimerSeconds: 0,
		recordedBlobs: [],
		belowText: this.messages.initialMessage
	}

	componentDidMount = () => {
		// triggers webcam permission dialog or initializes the webcam
		this.requestUserMedia()
		window.addEventListener('beforeunload', this.releaseWebcam())
	}
	
	componentWillUnmount() {
		this.releaseWebcam()
		window.removeEventListener('beforeunload', this.releaseWebcam())
	}

	// obtain webcam stream
	requestUserMedia() {
		captureUserMedia(stream => {
			if (stream) {
				this.setState({
					previewStream: stream,
					src: (window.URL || window.webkitURL).createObjectURL(stream)
				})
			} else {
				this.setState({ webcamFailed: true })
			}
		})
	}

	// start recording from the user's desktop webcam
	startRecording = () => {
		captureUserMedia(stream => {
			let mediaRecorder
			try {
				mediaRecorder = new MediaRecorder(
					stream,
					this.getMediaRecorderOptions()
				)
				mediaRecorder.ondataavailable = this.onDataAvailable
				mediaRecorder.start(10)

				this.startRecordingTimer()
				this.setState({
					recordingStream: stream,
					mediaRecorder,
					isRecording: true
				})
			} catch (e) {
				console.error('Exception while creating MediaRecorder: ' + e)
				return
			}
		})
	}

	// stop recording from the user's desktop webcam
	stopRecording() {
		this.state.mediaRecorder.stop()
		this.stopRecordingTimer()
		this.releaseRecordingStream()
		let superBuffer = new Blob(this.state.recordedBlobs, { type: 'video/webm' })
		this.setState({
			src: (window.URL || window.webkitURL).createObjectURL(superBuffer),
			isRecording: false,
			isRecordingPreview: true
		})
	}

	uploadOpinion = () => {
		if (!isEmailValid(this.state.email)) {
			this.setState({ belowText: this.messages.invalidEmail })
		} else {
			this.setState({
				isUploading: true,
				belowText: this.messages.uploading
			})
			Api.uploadOpinion(
				{
					recordedBlobs: this.state.recordedBlobs,
					email: this.state.email,
					taxonomy: this.props.taxonomy
				},
				res => {
					this.setState({
						isUploaded: true,
						belowText: this.messages.uploadedOpinionSuccess
					})
				}
			)
		}
	}
	
	// start interval that keeps track of recording duration
	startRecordingTimer() {
		var recordingTimerInterval = setInterval(
			this.increaseRecordingTimer.bind(this),
			1000
		)
		this.setState({ recordingTimerInterval: recordingTimerInterval })
	}

	stopRecordingTimer() {
		var recordingTimerInterval = this.state.recordingTimerInterval
		clearInterval(recordingTimerInterval)
	}

	formatRecordingTimer(i) {
		if (i < 10) i = '00:0' + i
		else i = '00:' + i
		return i
	}

	increaseRecordingTimer() {
		const maxRecordTime = 60
		if (this.state.recordingTimerSeconds < maxRecordTime) {
			this.setState(prevState => ({
				recordingTimerSeconds: prevState.recordingTimerSeconds + 1
			}))
		}
		if (this.state.recordingTimerSeconds === maxRecordTime) {
			this.stopRecording()
		}
	}

	// handle start, stop, and retake button clicks
	onRecordingControlButtonClicked = () => {
		if (!this.state.isRecording && !this.state.isRecordingPreview) {
			this.startRecording()
		} else if (!this.state.isRecording && this.state.isRecordingPreview) {
			this.resetCamera()
		} else if (this.state.isRecording) {
			this.stopRecording()
		}
	}

	onDataAvailable = event => {
		if (event.data && event.data.size > 0) {
			this.setState({
				recordedBlobs: [...this.state.recordedBlobs, event.data]
			})
		}
	}
	
	// reset camera to intial state
	resetCamera() {
		this.setState({
			isRecording: false,
			isRecordingPreview: false,
			isMuted: true,
			isPlaying: false,
			isPreviewActive: false,
			recordingTimerSeconds: 0,
			src: this.state.previewStream
				? (window.URL || window.webkitURL).createObjectURL(
					this.state.previewStream
				)
				: undefined
		})
	}
	
	// release all webcam streams so device camera light turns off
	releaseWebcam() {
		this.releasePreviewStream()
		this.releaseRecordingStream()
	}

	releasePreviewStream() {
		if (this.state.previewStream !== null) {
			this.state.previewStream.getAudioTracks().forEach(function(track) {
				track.stop()
			})
			this.state.previewStream.getVideoTracks().forEach(function(track) {
				track.stop()
			})
		}
	}

	releaseRecordingStream() {
		if (this.state.recordingStream !== null) {
			this.state.recordingStream.getAudioTracks().forEach(function(track) {
				track.stop()
			})
			this.state.recordingStream.getVideoTracks().forEach(function(track) {
				track.stop()
			})
		}
	}

	getMediaRecorderOptions = () => {
		var options = { mimeType: 'video/webm;codecs=vp9' }
		if (!MediaRecorder.isTypeSupported(options.mimeType)) {
			options = { mimeType: 'video/webm;codecs=vp8' }
			if (!MediaRecorder.isTypeSupported(options.mimeType)) {
				options = { mimeType: 'video/webm' }
				if (!MediaRecorder.isTypeSupported(options.mimeType)) {
					options = { mimeType: '' }
				}
			}
		}
		return options
	}

	getRecordingControlButtonImageUrl = () => {
		if (!this.state.isRecording && !this.state.isRecordingPreview) {
			return 'https://d1uck549nef0ok.cloudfront.net/button_record.png'
		} else if (!this.state.isRecording && this.state.isRecordingPreview) {
			return 'https://d1uck549nef0ok.cloudfront.net/button_redo.png'
		} else if (this.state.isRecording) {
			return 'https://d1uck549nef0ok.cloudfront.net/button_stop.png'
		}
	}
	
	getRecordingControlButtonTitle = () => {
		if (!this.state.isRecording && !this.state.isRecordingPreview) {
			return 'Start'
		} else if (!this.state.isRecording && this.state.isRecordingPreview) {
			return 'Retake'
		} else if (this.state.isRecording) {
			return 'Stop'
		}
	}
	
	handleInputChange = e => this.setState({ [e.target.name]: e.target.value })

	render() {
		const recordingControlButton = (
			<img
				id="record-button"
				src={this.getRecordingControlButtonImageUrl()}
				height={48}
				width={48}
				title={this.getRecordingControlButtonTitle()}
				onClick={this.onRecordingControlButtonClicked}
			/>
		)

		const recordButtonContainer = (
			<div className="yp_widget__record-modal-btn">
				<span style={{ paddingRight: '30px' }} id="yp_widget_start-text">
					{this.getRecordingControlButtonTitle()}
				</span>
				<div id="record-stop">{recordingControlButton}</div>
				<div id="recording-time" title="Status">
					{this.formatRecordingTimer(this.state.recordingTimerSeconds)}
				</div>
			</div>
		)

		const uploadCompleteButton = (
			<div className="yp_widget__thanks-modal-btn">
				<span id="thanks-text">{this.messages.thankYou}</span>
			</div>
		)

		const callToActionButton =
			this.state.isUploading || this.state.isUploaded
				? uploadCompleteButton
				: recordButtonContainer

		return (
			<div>
				<div
					id="record-vid-div"
					style={{ display: 'block', textAlign: 'center' }}
					className="form-group"
				>
					<video
						id="webcam-video"
						className="webcam-video"
						poster="https://widget.youplus.com/rw/img/youplus_logo_circle.svg"
						loop
						autoPlay
						playsInline
						muted={this.state.isMuted}
						src={this.state.src}
					/>

					<div id="controls">
						<hr className="yp_widget_hr_modal" />
						{callToActionButton}
						<div>
							<a href="https://www.youplus.com/" target="_blank">
								<img
									alt="youplus website"
									src="https://s3-ap-southeast-1.amazonaws.com/widget-ind-assets/img/youplus_logo_powered.png"
									className="yp-powered"
								/>{' '}
							</a>
						</div>
						{!this.state.isRecording &&
							this.state.isRecordingPreview && (
								<div>
									<div
										id="below-text"
										style={{
											color:
												this.state.belowText === this.messages.invalidEmail
													? 'red'
													: 'black'
										}}
									>
										{this.state.belowText}
									</div>
									{!this.state.isUploaded &&
										(!this.state.isUploading && (
											<div id="email-field">
												<input
													id="email-id"
													className="email-style"
													name="email"
													type="email"
													placeholder="Please enter your email address"
													onChange={this.handleInputChange}
												/>
												<div
													id="upload-button"
													className="btn btn-blue"
													onClick={this.uploadOpinion}
												>
													Submit
												</div>
											</div>
										))}
								</div>
							)}
						<div id="record-vid-div-below-text" />
					</div>
				</div>
			</div>
		)
	}
}
