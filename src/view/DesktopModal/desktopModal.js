import React from 'react'

import Modal from '../../components/Modal/modal'
import Playlist from '../../components/Playlist/playlist'

import Star from '../../static/star-outline.svg'
import StarFilled from '../../static/star.svg'
import PlayIcon from '../../static/play_arrow.svg'
import ReplayIcon from '../../static/replay.svg'
import { getVideoURL } from '../../utils/utils'
import { Player } from '../../components/player/video-react'
import '../../components/player/video-react.min.css'

import './__style.css'

export default class DesktopModal extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			data: props.data,
			loader: false,
			replay: false
		}
	}
	
	cancelLoader = () => {
		this.setState({
			replay: true,
			loader: false
		})
	}

	playNext = () => {
		if (this.state.loader) {
			const { playlistData } = this.props
			const { data } = this.state
			//Autoplay update state
			let playIndex
			playlistData.forEach((video, i) => {
				if(video.question.video_info[0].q_tags_id === data.video_info[0].q_tags_id) {
					playIndex = i+1
				}
			})
			let newData = playlistData[playIndex] ? playlistData[playIndex].question : playlistData[0].question
			this.setState({
				data: newData,
				loader: false,
				replay: false,
			})
		}
	}

	onClose = () => {
		this.props.onClose()
	}

	onPlay = video => {
		const newData = Object.assign({}, this.state.data, { video_info: [video] })
		this.setState({
			data: newData,
			loader: false
		})
	}

	onReplay = () => {
		this.refs.player.play()
		this.setState({
			replay: false
		})
	}

	onEnded = () => {
		this.setState({ loader: true })
		setTimeout(this.playNext, 3000)
	}

	render() {
		const { data } = this.state
		const { show, playlistData } = this.props
		const { question, video_info, video } = data
		//If there is video_url then the plays directly else will append video_id
		//to the api to play video
		let url = getVideoURL(video_info[0])
		//let url = getVideoURL(`${video_info[0] ? video_info[0] : data.video_info[0]}`)
		//Checks if there is #t in the url, if it is there splits it & appends it to
		//an array if not start & end are default values. The values are passed
		//as props to the player
		let start, end
		let videoTimeRange =
			url.indexOf('#t') !== -1
				? url.substring(url.indexOf('#t') + 3).split(',')
				: null
		if (videoTimeRange) {
			start = JSON.parse(videoTimeRange[0])
			end = JSON.parse(videoTimeRange[1])
		}

		let filledStar = []
		let outlineStar = []
		for (let i = 0; i < video_info[0].rating_by_user; i++) {
			filledStar.push(
				<img className="starIcon" src={StarFilled} alt="Ratings" key={i} />
			)
		}
		for (let i = 0; i < 5 - video_info[0].rating_by_user; i++) {
			outlineStar.push(
				<img className="starIcon" src={Star} alt="Ratings" key={i} />
			)
		}
		const responsive = {
			0: { items: 1 },
			300: { items: 2 },
			450: { items: 3 },
			900: { items: 4 },
			1050: { items: 5 },
			1600: { items: 6 },
			2048: {items: 7},
		}
		let playIndex
		playlistData.forEach((video, i) => {
			if(video.question.video_info[0].q_tags_id === data.video_info[0].q_tags_id) {
				playIndex = i+1
			}
		})
		let nextVideo = playlistData[playIndex] ? playlistData[playIndex].question : playlistData[0].question
		debugger
		return (
			<Modal show={show} onClose={this.onClose} isDesktop={true}>
				<div className="modal-body">
					<div className="modal-player">
						<Player ref="player" onEnded={this.onEnded} fluid={false} height="300px" startTime={start} endTime={end} src={url} autoPlay/>
						{(this.state.loader && !this.state.replay) && (
							<div className="loader-upnext" style={{display:'inline', position: 'absolute', bottom: 40, left: 0, color: 'white', margin: 10}}>
								<img src={nextVideo.video_info[0].thumbnail_image_url} alt="thumbnail" style={{height: 50, width: 50}} />
								<span style={{marginLeft: 5}}>Coming up next</span>
							</div>
						)}
						{(this.state.loader && !this.state.replay) && (
							<div className="player-loader">
								<div id="countdown" onClick={this.playNext}>
									<div id="countdown-number">
										<img
											style={{
												width: 40,
												height: 40,
												borderRadius: '50%',
												marginTop: 10
											}}
											src={PlayIcon}
											alt=""
										/>
									</div>
									<svg>
										<circle r="27" cx="30" cy="30" />
									</svg>
								</div>
								<div onClick={this.cancelLoader} className="player-loader-cancel" style={{cursor: 'pointer', color:'white', backgroundColor: 'lightgray'}}>
									<span>Cancel</span>
								</div>
							</div>
						)}
						{(!this.state.loader && this.state.replay) && (
							<div className="player-loader" onClick={this.onReplay} style={{color: 'white', cursor: 'pointer'}}>
								<img style={{height: 50}} src={ReplayIcon} alt="Replay" />
								Replay
							</div>
						)}
					</div>
					<div className="modal-info">
						<div className="modal-info-user">
							<div className="modal-info-user-thumbnail">
								<img
									className="modal-info-profile-pic"
									src={video_info[0].user_profile_picture}
									alt=""
								/>
							</div>
							<div style={{ color: '#00aeef' }}>
								{video_info[0].user_name}
								<div style={{ display: 'flex' }}>
									{filledStar}
									{outlineStar}
								</div>
							</div>
						</div>
						<div className="modal-info-question">
							{question}
						</div>
						<div className="modal-info-video">
							{video_info[0].transcription_script}
						</div>
					</div>
				</div>
				<div className="modal-playist">
					<Playlist
						width="92%"
						isDesktop={true}
						data={playlistData}
						video={video_info[0]}
						onClick={this.onPlay}
						responsive={responsive}
					/>
				</div>
			</Modal>
		)
	}
}
