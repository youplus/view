import React from 'react'

import Card from '../../components/Card/card.js'
import Loader from '../../components/Loader/loader.js'
import AliceCarousel from 'react-alice-carousel'
import RecordFooter from '../../components/RecordFooter/recordFooter.js'

import ArrowLeft from '../../static/arrow-left.png'
import ArrowRight from '../../static/arrow-right.png'

import './__style.css'

export default class Carousel extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			playIndex: 0
		}
	}

	onPlay = data => {
		this.props.toggleModal(data)
		this.setState({
			playIndex: null
		})
	}

	toggleRecord = () => {
		this.props.toggleRecord()
	}

	onVideoEnded = () => {
		this.setState ({
			playIndex: this.state.playIndex+1
		})
	}

	render() {
		const { data, isMobile } = this.props
		const { playIndex } = this.state
		
		// Specifies the no.of cards to be shown based on the width of the screen i.e, 0/500/1000/1300px
		const responsive = {
			0: { items: 1 },
			450: { items: 2 },
			900: { items: 3 },
			1200: { items: 4 },
			1600: { items: 5 }
		}

		let cards = []
		data && data.forEach((data, i) => {
			cards.push(
				<Card
					onPlay={this.onPlay}
					device="Desktop"
					key={i}
					data={data.question}
					playVideo={(i===playIndex && !isMobile) ? true : false}
					onVideoEnded={this.onVideoEnded}
				/>
			)
		})
		if (!data) {
			for (let i = 0; i < 4; i++) {
				cards.push(<Loader />)
			}
		}
		const arrowIconCName = isMobile ? 'carousel-view-arrow-mobile' : 'carousel-view-arrow-desktop'
		return (
			<div>
				<div className="carousel-view">
					<img
						className={arrowIconCName}
						src={ArrowLeft}
						onClick={() => this.Carousel._slidePrev()}
						alt="Left Arrow"
					/>
					<div style={{width: isMobile ? '80%' : '96%'}}>
						<AliceCarousel
							style={{ padding: 0 }}
							items={cards}
							responsive={responsive}
							dotsDisabled={true}
							buttonsDisabled={true}
							ref={el => (this.Carousel = el)}
							keysControlDisabled={true}
							startIndex={playIndex}
						/>
					</div>
					<img
						className={arrowIconCName}
						src={ArrowRight}
						onClick={() => this.Carousel._slideNext()}
						alt="Right Arrow"
					/>
				</div>
				<RecordFooter onRecord={this.toggleRecord} />
			</div>
		)
	}
}
