import React from 'react'

import PhoneModal from '../../components/MobileModal/mobileModal'
import Playlist from '../../components/Playlist/playlist'

import Star from '../../static/star-outline.svg'
import StarFilled from '../../static/star.svg'
import PlayIcon from '../../static/play_arrow.svg'
import { getVideoURL } from '../../utils/utils'
import { Player } from '../../components/player/video-react'
import '../../components/player/video-react.min.css'

import './__style.css'

export default class MobileModal extends React.Component {
	constructor(props) {
		super(props)
		// debugger
		// let locOrientation = window.screen.lockOrientation || window.screen.mozLockOrientation || window.screen.msLockOrientation || window.screen.orientation.lock
		// locOrientation('landscape')
		this.state = {
			data: props.data,
			loader: false,
		}
	}

	playNext = () => {
		const { playlistData } = this.props
		const { data } = this.state
		//Autoplay update state
		let playIndex
		playlistData.forEach((video, i) => {
			if(video.question.video_info[0].q_tags_id === data.video_info[0].q_tags_id) {
				playIndex = i+1
			}
		})
		let newData = playlistData[playIndex] ? playlistData[playIndex].question : playlistData[0].question
		this.setState({
			data: newData,
			loader: false
		})
	}

	onClose = () => {
		this.props.onClose()
	}

	onPlay = video => {
		const newData = Object.assign({}, this.state.data, { video_info: [video] })
		this.setState({
			data: newData
		})
	}

	onEnded = () => {
		this.setState({ loader: true })
		setTimeout(this.playNext, 3000)
	}

	render() {
		const { data } = this.state
		const { show, playlistData } = this.props
		const { question, video_info } = data
		//If there is video_url then the plays directly else will append video_id
		//to the api to play video
		let url = getVideoURL(video_info[0])
		//Checks if there is #t in the url, if it is there splits it & appends it to
		//an array if not start & end are default values. The values are passed
		//as props to the player
		let start, end
		let videoTimeRange =
			url.indexOf('#t') !== -1
				? url.substring(url.indexOf('#t') + 3).split(',')
				: null
		if (videoTimeRange) {
			start = videoTimeRange[0]
			end = videoTimeRange[1]
		}
		let filledStar = []
		let outlineStar = []
		for (let i = 0; i < video_info[0].rating_by_user; i++) {
			filledStar.push(
				<img className="mobile-starIcon" src={StarFilled} alt="Ratings" key={i} />
			)
		}
		for (let i = 0; i < 5 - video_info[0].rating_by_user; i++) {
			outlineStar.push(
				<img className="mobile-starIcon" src={Star} alt="Ratings" key={i} />
			)
		}
		const responsive = {
			0: { items: 0 },
			100: {items: 1 },
			250: { items: 2 },
			350: { items: 3 },
			450: { items: 4 },
			550: { items: 5 },
			650: { items: 6 },
			750: { items: 7 }
		}
		return (
			<PhoneModal show={show} onClose={this.onClose}>
				<div className="mobile-modal-body">			
					<div className="mobile-modal-player">
						<Player onEnded={this.onEnded} fluid={false} height="300px" startTime={start} endTime={end} src={url} autoPlay playsInline/>
						{this.state.loader && (
							<div id="mobile-countdown">
								<div id="mobile-countdown-number">
									<img
										style={{
											width: '50px',
											height: '50px',
											borderRadius: '50%',
											marginTop: '15px'
										}}
										src={PlayIcon}
										alt=""
									/>
								</div>
								<svg className="loading-circle">
									<circle r="37" cx="40" cy="40" />
								</svg>
							</div>
						)}
					</div>
					<div className="mobile-modal-footer">
						<div className="mobile-modal-info">
							<div className="mobile-modal-info-user">
								<div className="mobile-modal-info-user-thumbnail">
									<img
										className="mobile-modal-profilepic"
										src={video_info[0].user_profile_picture}
										alt=""
									/>
								</div>
								<div className="mobile-modal-name">
									<div>{video_info[0].user_name}</div>
									<div style={{ display: 'flex' }}>
										{filledStar}
										{outlineStar}
									</div>
									<div className="mobile-card-container-details-options">
										<div className="views">
											views: <strong>{video_info[0].view_count}</strong>
										</div>
										<div className="duration">
                    						sec: <strong>{video_info[0].duration || '00:15'}</strong>
										</div>
									</div>
								</div>
							</div>
							<div className="mobile-modal-question">
								{question}
							</div>
							<div className="mobile-modal-info-video">
								{video_info[0].transcription_script}
								{/* I like this type of dress because it does not have buttons I feel like to play with this a lot and sleep with this because it's so comfortable and I want to wear the whole day thank you bye.
								I like this type of dress because it does not have buttons I feel like to play with this a lot and sleep with this because it's so comfortable and I want to wear the whole day thank you bye.
								I like this type of dress because it does not have buttons I feel like to play with this a lot and sleep with this because it's so comfortable and I want to wear the whole day thank you bye. */}
							</div>
						</div>
						<div className="mobile-modal-playlist">
							<Playlist
								marginLeft="20px"
								marginRight="10px"
								marginBottom="10px"
								isMobile={true}
								data={playlistData}
								video={video_info[0]}
								onClick={this.onPlay}
								responsive={responsive}
							/>
						</div>
					</div>
				</div>
			</PhoneModal>
		)
	}
}
