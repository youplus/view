import React, { Component } from 'react'
// import { YOUPLUS_CONFIG, REC_IMG } from './utils/const';

import './App.css'
import Carousel from './view/Carousel/carousel'
import DesktopModal from './view/DesktopModal/desktopModal'
import MobileModal from './view/MobileModal/mobileModal'
import MobileRecord from './view/mobileRecord'
import DesktopRecord from './view/desktopRecord'

import Media from 'react-media'

class App extends Component {
	shuffle = sourceArray => {
		for (var i = 0; i < sourceArray.length - 1; i++) {
			var j = i + Math.floor(Math.random() * (sourceArray.length - i))

			var temp = sourceArray[j]
			sourceArray[j] = sourceArray[i]
			sourceArray[i] = temp
		}
		return sourceArray
	}

	constructor(props) {
		super(props)
		this.state = {
			isShowRecord: false,
			contentObj: null,
			isShowModal: false,
			playerData: null
		}
	}

	toggleModal = playerData => {
		this.setState({
			playerData: playerData,
			isShowModal: !this.state.isShowModal
		})
	}

	toggleRecord = () => {
		this.setState({
			isShowRecord: !this.state.isShowRecord
		})
	}

	componentDidMount = () => {
		let self = this
		// let key = 'https://widget.youplus.com/infibeam/infiiphone7.html';
		// let key =
		//   new RegExp('[?&]' + 'link_url' + '=([^&#]*)').exec(
		//     window.location.href
		//   )[1] || 0;
		// let query =
		//   new RegExp('[?&]' + 'search_query' + '=([^&#]*)').exec(
		//     window.location.href
		//   )[1] || 0;
		// let query = 0;
		// let loadingEle = document.getElementById('loading');
		// let final_content_url =
		//   YOUPLUS_CONFIG.get_questions_url + key + '&search_query=' + query;

		let newURl = 'https://ind-sapi.yupl.us/v1.7/app/live_opinion/get_questions?url=https://www.nuteez.com/&search_query=0'
		// let newURl =
		// 	'https://ind-devapi.yupl.us/v.staging/app/live_opinion/get_questions?url=https://widgetin.yupl.us/tcns/FashionShopping.html&search_query=0'
		fetch(newURl, { method: 'GET', async: true, timeout: 40000 })
			.then(response => response.json())
			.then(res => {
				console.log(res)
				if (res.questions && res.questions.length > 0) {
					let questions = res.questions
					let taxonomy = res.taxonomy
					let videoSet = []
					let isOrdered = res.ordered
					questions.forEach(function(question) {
						question.video_info.forEach(function(video) {
							videoSet.push({ question, video })
						})
					})
					if (isOrdered) {
						self.setState({ contentObj: videoSet, taxonomy: taxonomy })
					} else {
						self.setState({
							contentObj: this.shuffle(videoSet),
							taxonomy: taxonomy
						})
					}
				} else {
					self.adjustHeight()
				}
			})
			.catch(function(err) {
				console.log('error', err)
			})
	}

	render() {
		const { contentObj, isShowModal, playerData, isShowRecord } = this.state
		return (
			<div className="Widget" style={{ width: '100%' }}>
				<div
					className="slider-title"
					style={{ fontSize: 'larger', fontWeight: 'bold', textAlign: 'left' }}>
					Video Opinions
				</div>
				{/* Note: Made iphoneX the breakpoint @812px width. 
					In iPad portrait shows Mobile and in landscape shows Desktop */}
				<Media query="(max-width: 900px)">
					{matches =>
						matches ? (
							<div>
								<Carousel isMobile={true} toggleRecord={this.toggleRecord} toggleModal={this.toggleModal} data={contentObj} />
								{isShowModal && (<MobileModal
									playlistData={contentObj}
									data={playerData}
									show={isShowModal}
									onClose={this.toggleModal}
								/>)}
							</div>
						) : (
							<div>
								<Carousel toggleRecord={this.toggleRecord} toggleModal={this.toggleModal} data={contentObj} />
								{isShowModal && (<DesktopModal
									playlistData={contentObj}
									data={playerData}
									show={isShowModal}
									onClose={this.toggleModal}
								/>)}
							</div>
						)
					}
				</Media>
				{isShowRecord && (
					<Media query={{ maxWidth: 900}}>
						{matches =>
							matches ? (
								<MobileRecord
									show={isShowRecord}
									onClose={this.toggleRecord}
									taxonomy={this.state.taxonomy}
								/>
							) : (
								<DesktopRecord
									show={isShowRecord}
									onClose={this.toggleRecord}
									taxonomy={this.state.taxonomy}
								/>
							)
						}
					</Media>
				)}
			</div>
		)
	}
}

export default App
