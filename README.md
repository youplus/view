This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>

## Folder Structure

After creation, your project should look like this:

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    App.css
    App.js
    App.test.js
    index.css
    index.js
    components/
      card.js
      loader.js
      slider.js
    static/
      play-ico.png
      star-outline.svg
      star.svg
    utils/
      const.js
    view/
      carousel.js
```

For the project to build, **these files must exist with exact filenames**:

- `public/index.html` is the page template;
- `src/index.js` is the JavaScript entry point.

You can delete or rename the other files.

You may create subdirectories inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack.<br>
You need to **put any JS and CSS files inside `src`**, otherwise Webpack won’t see them.

**Structure Definition**:-

- `components` - This folder contains all the Pure React components
- `static` - This folder contains all the static files like images, icons, etc
- `utils` - This folder contains the utility files required by the App to connect to proper API's, get the correct BASE_URL, etc
- `view` - This folder contains each view of the Widget like Carousel, Modal, etc
